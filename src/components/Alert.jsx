import { IconButton } from "@material-ui/core";
import { Close } from "@material-ui/icons";
import { Alert as ALERT } from "@material-ui/lab";

/*  You will have to pass two parameters, one to show or not the message and the other will be the message. */

const Alert = (props) => {
 return (
  <ALERT
   variant="filled"
   severity="error"
   action={
    <IconButton
     aria-label="close"
     color="inherit"
     size="small"
     onClick={() => {
      props.state(false);
     }}>
     <Close fontSize="inherit" />
    </IconButton>
   }>
   {props.messageError}
  </ALERT>
 );
};

export default Alert;
