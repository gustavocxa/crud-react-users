import { storage } from "../firebase/Setting";
import { db } from "../firebase/Setting";

function FileUpload(userID, file) {
 const storageRef = storage.ref(`ProfilePicture/${file.name}`);
 storageRef.put(file).then(function (snapshot) {
  snapshot.ref.getDownloadURL().then(function (downloadURL) {
   db.collection("users").doc(userID).update({ profilePicture: downloadURL }).then(console.log("Foto de perfil actualizado"));
  });
 });
}

export default FileUpload;
