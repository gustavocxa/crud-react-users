import { useState } from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { PrivateRoute, PublicRoute } from "./SafeRoute";
import Header from "./Header";
import Footer from "./Footer";
import SignIn from "../views/SignIn";
import SignUp from "../views/SignUp";
import Users from "../views/Users";
import UpdateUser from "../views/UpdateUser";
import Copyrigth from "./Copyrigth";
import { ThemeProvider, createMuiTheme } from "@material-ui/core";

function Router() {
 const [darkMode, setDarkMode] = useState(false);
 const darkTheme = createMuiTheme({ palette: { type: "dark", primary: { main: "#61DBFB" } } });
 const lightTheme = createMuiTheme({ palette: { type: "light" } });

 return (
  <BrowserRouter>
   <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
    <Header darkMode={darkMode} setDarkMode={setDarkMode} />
    <Switch>
     <Route exact path="/" component={Users} />

     <Route exact path="/Home" component={Users} />

     <PublicRoute exact path="/SignUp">
      <SignUp />
     </PublicRoute>

     <PublicRoute exact path="/SignIn">
      <SignIn />
     </PublicRoute>

     <PrivateRoute exact path="/UpdateUser">
      <UpdateUser />
     </PrivateRoute>

     <Route exact path="/Copyrigth" component={Copyrigth} />

     <Route>
      <Redirect to="/" />
     </Route>
    </Switch>

    <Footer />
   </ThemeProvider>
  </BrowserRouter>
 );
}

export default Router;
