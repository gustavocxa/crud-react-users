import { useEffect } from "react";
import { Route, useHistory } from "react-router-dom";
import { auth } from "../firebase/Setting";
import Loading from "./Loading";

export function PrivateRoute({ children, ...rest }) {
 const history = useHistory();

 useEffect(() => {
  auth.onAuthStateChanged((user) => {
   user ? history.push(rest.path) : history.push("/");
  });
 }, [history]);
 return <Route {...rest} render={({ location }) => (auth.currentUser ? children : <Loading />)} />;
}

export function PublicRoute({ children, ...rest }) {
 const history = useHistory();

 useEffect(() => {
  auth.onAuthStateChanged((user) => {
   user ? history.push("/") : history.push(rest.path);
  });
 }, [history, rest.path]);

 return <Route {...rest} render={({ location }) => (auth.currentUser ? <Loading /> : children)} />;
}
