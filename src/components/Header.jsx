import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { signOut } from "../firebase/Authentication";
import { auth } from "../firebase/Setting";
import { AppBar, Toolbar, BottomNavigation, BottomNavigationAction, Box, IconButton, Paper } from "@material-ui/core";
import { Home, PersonAdd, Lock, Edit, ExitToApp, BrightnessLow, BrightnessHigh } from "@material-ui/icons";
import logo from "../images/logo.svg";

function Header(props) {
 /* value: current position of the route in the menu */
 const [value, setValue] = useState();
 const [stateAtuth, setStateAuth] = useState();

 let history = useHistory();
 let location = useLocation();

 function PUSH() {
  history.push("/");
 }

 function currrentRoute() {
  switch (location.pathname) {
   case "/":
    setValue(0);
    break;

   case "/Home":
    setValue(0);
    break;

   case "/SignUp":
    setValue(1);
    break;

   case "/SignIn":
    setValue(2);
    break;

   case "/UpdateUser":
    setValue(1);
    break;

   case "/Copyrigth":
    setValue(null);
    break;

   default:
    break;
  }
 }

 useEffect(() => {
  currrentRoute();
 }, [location]);

 auth.onAuthStateChanged((user) => {
  setStateAuth(user);
 });

 return (
  <Paper>
   <AppBar position="static" style={{ flexGrow: "1" }} color="default">
    <Toolbar>
     <img src={logo} className="App-logo" alt="logo" draggable="false" />
     <h1
      style={{
       alignContent: "center",
       userSelect: "none",
       flexGrow: "1",
      }}>
      CRUD
     </h1>
     <Box display="flex" flexDirection="row-reverse">
      {props.darkMode ? (
       <IconButton
        aria-label="Color"
        onClick={() => {
         props.setDarkMode(false);
        }}>
        <BrightnessHigh fontSize="large" />
       </IconButton>
      ) : (
       <IconButton
        aria-label="Color"
        onClick={() => {
         props.setDarkMode(true);
        }}>
        <BrightnessLow fontSize="large" style={{ color: "black" }} />
       </IconButton>
      )}
     </Box>
    </Toolbar>
   </AppBar>

   {stateAtuth ? (
    <BottomNavigation
     showLabels
     value={value}
     onChange={(event, newValue) => {
      setValue(newValue);
     }}>
     <BottomNavigationAction
      label="Home"
      icon={<Home />}
      onClick={() => {
       history.push("/Home");
      }}
     />

     <BottomNavigationAction
      label="Update User"
      icon={<Edit />}
      onClick={() => {
       history.push("/UpdateUser");
      }}
     />

     <BottomNavigationAction
      label="Sign Out"
      icon={<ExitToApp />}
      onClick={() => {
       setValue(0);
       signOut(PUSH);
      }}
     />
    </BottomNavigation>
   ) : (
    <BottomNavigation
     showLabels
     value={value}
     onChange={(event, newValue) => {
      setValue(newValue);
     }}>
     <BottomNavigationAction
      label="Home"
      icon={<Home />}
      onClick={() => {
       history.push("/Home");
      }}
     />

     <BottomNavigationAction
      label="Sign Up"
      icon={<PersonAdd />}
      onClick={() => {
       history.push("/SignUp");
      }}
     />

     <BottomNavigationAction
      label="Sign In"
      icon={<Lock />}
      onClick={() => {
       history.push("/SignIn");
      }}
     />
    </BottomNavigation>
   )}
  </Paper>
 );
}

export default Header;
