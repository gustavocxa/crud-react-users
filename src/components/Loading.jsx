import { Box } from "@material-ui/core";
import logo from "../images/logo.svg";

function Loading() {
 return (
  <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center" minHeight="60vh">
   <img src={logo} className="App-logo-loading" alt="logo" draggable="false" />
  </Box>
 );
}

export default Loading;
