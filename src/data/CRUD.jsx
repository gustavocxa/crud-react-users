import { db, auth } from "../firebase/Setting";
import FileUpload from "../components/FileUpload";

export function CreateUser(props, pushHome, setAlertErrorAuth, setMessageError) {
 let data = {
  username: props.username,
  name: props.name,
  lastname: props.lastname,
  email: props.email,
  password: props.password,
 };

 auth
  .createUserWithEmailAndPassword(data.email, data.password)
  .then((userCredential) => {
   db.collection("users").doc(data.email).set(data);
   pushHome();
  })
  .catch((error) => {
   setMessageError(error.message);
   setAlertErrorAuth(true);
  });
}

export function UpdateUser(props, userID, srcProfilePicture, pushViewUser) {
 let data = {
  username: props.username,
  name: props.name,
  lastname: props.lastname,
  email: props.email,
  password: props.password,
  profilePicture: "",
 };
 db.collection("users").doc(userID).set(data).then(pushViewUser);
 FileUpload(userID, srcProfilePicture);
}

export function DeleteUser(id, pushViewsUsers) {
 /* id = email */
 auth.currentUser
  .delete()
  .then(db.collection("users").doc(id).delete().then(pushViewsUsers))
  .catch((error) => {
   console.log(error);
  });
}
