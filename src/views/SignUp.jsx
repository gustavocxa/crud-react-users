import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { CreateUser } from "../data/CRUD";
import Alert from "../components/Alert";
/* import { ValidatorForm, TextValidator } from "react-material-ui-form-validator"; */
import { Link, Button, CssBaseline, TextField, FormControlLabel, Grid, Typography, Container, Checkbox, Paper } from "@material-ui/core";
import useStyles from "../styles/css";

function SignUp() {
 const css = useStyles();
 const [user, setUser] = useState([]);
 const [alertErrorAuth, setAlertErrorAuth] = useState(false);
 const [messageError, setMessageError] = useState("");

 let history = useHistory();

 function pushHome() {
  history.push("/");
 }

 function handleSubmit(e) {
  e.preventDefault();
  CreateUser(user, pushHome, setAlertErrorAuth, setMessageError);
 }

 return (
  <Container component="main" maxWidth="xs">
   <Paper>
    <CssBaseline />

    <div className={css.paper}>
     <Typography component="h1" variant="h5" color="primary">
      Sign Up
     </Typography>

     <form className={css.form} onSubmit={handleSubmit}>
      <Grid container spacing={2}>
       <Grid item xs={12}>
        <TextField
         id="signUpUserName"
         name="userName"
         label="User Name"
         autoComplete="Name"
         variant="outlined"
         fullWidth
         required
         autoFocus
         onChange={(event) => {
          setUser((state) => ({
           ...state,
           username: event.target.value,
          }));
         }}
        />
       </Grid>

       <Grid item xs={12} sm={6}>
        <TextField
         id="signUpName"
         name="firstName"
         label="First Name"
         autoComplete="First name"
         variant="outlined"
         fullWidth
         required
         onChange={(event) => {
          setUser((state) => ({
           ...state,
           name: event.target.value,
          }));
         }}
        />
       </Grid>

       <Grid item xs={12} sm={6}>
        <TextField
         id="signUpLastName"
         name="lastName"
         label="Last Name"
         autoComplete="lname"
         variant="outlined"
         fullWidth
         required
         onChange={(event) => {
          setUser((state) => ({
           ...state,
           lastname: event.target.value,
          }));
         }}
        />
       </Grid>

       <Grid item xs={12}>
       {alertErrorAuth ? <Alert state={setAlertErrorAuth} messageError={messageError} /> : null}
       </Grid>

       <Grid item xs={12}>
        <TextField
         id="signUpEmail"
         name="email"
         label="Email Address"
         autoComplete="email"
         type="email"
         variant="outlined"
         fullWidth
         required
         onChange={(event) => {
          setUser((state) => ({
           ...state,
           email: event.target.value,
          }));
         }}
        />
       </Grid>

       <Grid item xs={12}>
        <TextField
         id="signUpPassword"
         name="password"
         title="Escribir una contraseña mayor o igual de 7 digitos."
         label="Password"
         type="password"
         pattern=".{7,}"
         variant="outlined"
         fullWidth
         required
         autoComplete="current-password"
         onChange={(event) => {
          setUser((state) => ({
           ...state,
           password: event.target.value,
          }));
         }}
        />
       </Grid>

       <Grid item xs={12}>
        <FormControlLabel control={<Checkbox value="allowExtraEmails" color="primary" />} label="I want to receive inspiration, marketing promotions and updates via email." required />
       </Grid>
      </Grid>

      <Button type="submit" variant="contained" color="primary" className={css.submit} fullWidth>
       Sign Up
      </Button>

      <Grid container justify="flex-end">
       <Grid item>
        <Link
         style={{ cursor: "pointer" }}
         onClick={() => {
          history.push("/SignIn");
         }}>
         Already have an account? Sign in
        </Link>
       </Grid>
      </Grid>
     </form>
    </div>
   </Paper>
  </Container>
 );
}

export default SignUp;
