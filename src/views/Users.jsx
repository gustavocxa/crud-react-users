import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { auth, db } from "../firebase/Setting";
import { Container, CssBaseline, List, ListItem, ListItemSecondaryAction, ListItemText, Avatar, IconButton, Box, Paper } from "@material-ui/core";
import { Edit } from "@material-ui/icons";
import useStyles from "../styles/css";

function Users(props) {
 const css = useStyles();
 const history = useHistory();
 const [users, setUsers] = useState([]);

 useEffect(() => {
  /* GET USERS */
  db.collection("users").onSnapshot((data) => {
   setUsers(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
  });
 }, []);

 return (
  <Container component="main">
   <Paper>
    <CssBaseline />

    <Box display="flex" justifyContent="center" m={3}>
     <List align="right" className={css.listUsers}>
      {users.length > 0 ? (
       users.map((user) => (
        <ListItem key={user.id} button>
         <Avatar alt="Profile" src={user.profilePicture} className={css.avatarUsers} />

         <ListItemText id={user.id} primary={user.name} secondary={`@${user.username}`} />

         {auth.currentUser ? (
          auth.currentUser.email === user.email ? (
           <ListItemSecondaryAction>
            <IconButton
             edge="end"
             aria-label="edit"
             color="primary"
             onClick={() => {
              history.push("UpdateUser");
             }}>
             <Edit />
            </IconButton>
           </ListItemSecondaryAction>
          ) : null
         ) : null}
        </ListItem>
       ))
      ) : (
       <ListItem>
        <ListItemText id="NotUsers" primary="Loading... No users yet..." />
       </ListItem>
      )}
     </List>
    </Box>
   </Paper>
  </Container>
 );
}
export default Users;
